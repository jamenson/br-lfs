# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-03-11 19:30+0000\n"
"PO-Revision-Date: 2023-03-24 05:47+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/linux-from-scratch-11-3/prologue_dedication/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.16.2\n"

#. type: Content of: <dedication><title>
#: lfs-en/prologue/dedication.xml:9
#, xml-text
msgid "Dedication"
msgstr "Dedicação"

#. type: Content of: <dedication><para>
#: lfs-en/prologue/dedication.xml:10
#, xml-text
msgid "This book is dedicated"
msgstr "Este livro é dedicado"
