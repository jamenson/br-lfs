# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-03-05 01:51-0300\n"
"PO-Revision-Date: 2023-10-05 17:58+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/linux-from-scratch/chapter08_man-db/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.0.2\n"

#. type: Content of: <sect1><sect1info><address>
#: lfs-en/chapter08/man-db.xml:14
#, no-wrap, xml-text
msgid "&man-db-url;"
msgstr "&man-db-url;"

#. type: Content of: <sect1><sect1info>
#: lfs-en/chapter08/man-db.xml:12
#, xml-text
msgid ""
"<productname>man-db</productname> <productnumber>&man-db-"
"version;</productnumber> <placeholder type=\"address\" id=\"0\"/>"
msgstr ""
"<productname>man-db</productname> <productnumber>&man-db-"
"version;</productnumber> <placeholder type=\"address\" id=\"0\"/>"

#. type: Content of: <sect1><title>
#: lfs-en/chapter08/man-db.xml:17
#, xml-text
msgid "Man-DB-&man-db-version;"
msgstr "Man-DB-&man-db-version;"

#. type: Content of: <sect1><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:20
#, xml-text
msgid "Man-DB"
msgstr "Man-DB"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/man-db.xml:26
#, xml-text
msgid ""
"The Man-DB package contains programs for finding and viewing man pages."
msgstr ""
"O pacote Man-DB contém aplicativos para encontrar e visualizar páginas de "
"manual."

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/man-db.xml:30
#, xml-text
msgid "&buildtime;"
msgstr "&buildtime;"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/man-db.xml:31
#, xml-text
msgid "&diskspace;"
msgstr "&diskspace;"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/man-db.xml:34
#, xml-text
msgid "&man-db-fin-sbu;"
msgstr "&man-db-fin-sbu;"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/man-db.xml:35
#, xml-text
msgid "&man-db-fin-du;"
msgstr "&man-db-fin-du;"

#. type: Content of: <sect1><sect2><title>
#: lfs-en/chapter08/man-db.xml:42
#, xml-text
msgid "Installation of Man-DB"
msgstr "Instalação do Man-DB"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/man-db.xml:44
#, xml-text
msgid "Prepare Man-DB for compilation:"
msgstr "Prepare Man-DB para compilação:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/man-db.xml:46
#, no-wrap, xml-text
msgid ""
"<userinput remap=\"configure\">./configure --prefix=/usr                         \\\n"
"            --docdir=/usr/share/doc/man-db-&man-db-version; \\\n"
"            --sysconfdir=/etc                     \\\n"
"            --disable-setuid                      \\\n"
"            --enable-cache-owner=bin              \\\n"
"            --with-browser=/usr/bin/lynx          \\\n"
"            --with-vgrind=/usr/bin/vgrind         \\\n"
"            --with-grap=/usr/bin/grap</userinput>"
msgstr ""
"<userinput remap=\"configure\">./configure --prefix=/usr                         \\\n"
"            --docdir=/usr/share/doc/man-db-&man-db-version; \\\n"
"            --sysconfdir=/etc                     \\\n"
"            --disable-setuid                      \\\n"
"            --enable-cache-owner=bin              \\\n"
"            --with-browser=/usr/bin/lynx          \\\n"
"            --with-vgrind=/usr/bin/vgrind         \\\n"
"            --with-grap=/usr/bin/grap</userinput>"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/man-db.xml:55
#, no-wrap, xml-text
msgid ""
"<userinput remap=\"configure\">./configure --prefix=/usr                         \\\n"
"            --docdir=/usr/share/doc/man-db-&man-db-version; \\\n"
"            --sysconfdir=/etc                     \\\n"
"            --disable-setuid                      \\\n"
"            --enable-cache-owner=bin              \\\n"
"            --with-browser=/usr/bin/lynx          \\\n"
"            --with-vgrind=/usr/bin/vgrind         \\\n"
"            --with-grap=/usr/bin/grap             \\\n"
"            --with-systemdtmpfilesdir=            \\\n"
"            --with-systemdsystemunitdir=</userinput>"
msgstr ""
"<userinput remap=\"configure\">./configure --prefix=/usr                         \\\n"
"            --docdir=/usr/share/doc/man-db-&man-db-version; \\\n"
"            --sysconfdir=/etc                     \\\n"
"            --disable-setuid                      \\\n"
"            --enable-cache-owner=bin              \\\n"
"            --with-browser=/usr/bin/lynx          \\\n"
"            --with-vgrind=/usr/bin/vgrind         \\\n"
"            --with-grap=/usr/bin/grap             \\\n"
"            --with-systemdtmpfilesdir=            \\\n"
"            --with-systemdsystemunitdir=</userinput>"

#. type: Content of: <sect1><sect2><variablelist><title>
#: lfs-en/chapter08/man-db.xml:67
#, xml-text
msgid "The meaning of the configure options:"
msgstr "O significado das opções do configure:"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:70
#, xml-text
msgid "<parameter>--disable-setuid</parameter>"
msgstr "<parameter>--disable-setuid</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:72
#, xml-text
msgid ""
"This disables making the <command>man</command> program setuid to user "
"<systemitem class=\"username\">man</systemitem>."
msgstr ""
"Isso desabilita tornar o aplicativo <command>man</command> setuid para o(a) "
"usuário(a) <systemitem class=\"username\">man</systemitem>."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:78
#, xml-text
msgid "<parameter>--enable-cache-owner=bin</parameter>"
msgstr "<parameter>--enable-cache-owner=bin</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:80
#, xml-text
msgid ""
"This changes ownership of the system-wide cache files to user <systemitem "
"class=\"username\">bin</systemitem>."
msgstr ""
"Isso muda a propriedade dos arquivos de cache de abrangência ao sistema para"
" o(a) usuário(a) <systemitem class=\"username\">bin</systemitem>."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:86
#, xml-text
msgid "<parameter>--with-...</parameter>"
msgstr "<parameter>--with-...</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:88
#, xml-text
msgid ""
"These three parameters are used to set some default programs.  "
"<command>lynx</command> is a text-based web browser (see BLFS for "
"installation instructions), <command>vgrind</command> converts program "
"sources to Groff input, and <command>grap</command> is useful for "
"typesetting graphs in Groff documents. The <command>vgrind</command> and "
"<command>grap</command> programs are not normally needed for viewing manual "
"pages. They are not part of LFS or BLFS, but you should be able to install "
"them yourself after finishing LFS if you wish to do so."
msgstr ""
"Esses três parâmetros são usados para configurar alguns aplicativos padrão. "
"<command>lynx</command> é um navegador da web baseado em texto (veja-se o "
"BLFS para instruções de instalação); <command>vgrind</command> converte "
"fontes de aplicativo para entrada gerada do Groff; e <command>grap</command>"
" é útil para tipografar gráficos em documentos do Groff. Os aplicativos "
"<command>vgrind</command> e <command>grap</command> normalmente não são "
"necessários para visualizar páginas de manual. Eles não são parte do LFS ou "
"do BLFS, mas você deveria ser capaz de instalá-los você mesmo(a) depois de "
"terminar o LFS, se desejar fazer isso."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:101
#, xml-text
msgid "<parameter>--with-systemd...</parameter>"
msgstr "<parameter>--with-systemd...</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:103
#, xml-text
msgid ""
"These parameters prevent installing unneeded systemd directories and files."
msgstr ""
"Esses parâmetros impedem a instalação de diretórios e arquivos "
"desnecessários do systemd."

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/man-db.xml:110
#, xml-text
msgid "Compile the package:"
msgstr "Compile o pacote:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/man-db.xml:112
#, no-wrap, xml-text
msgid "<userinput remap=\"make\">make</userinput>"
msgstr "<userinput remap=\"make\">make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/man-db.xml:114
#, xml-text
msgid "To test the results, issue:"
msgstr "Para testar os resultados, emita:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/man-db.xml:116
#, no-wrap, xml-text
msgid "<userinput remap=\"test\">make check</userinput>"
msgstr "<userinput remap=\"test\">make check</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/man-db.xml:118
#, xml-text
msgid "Install the package:"
msgstr "Instale o pacote:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/man-db.xml:120
#, no-wrap, xml-text
msgid "<userinput remap=\"install\">make install</userinput>"
msgstr "<userinput remap=\"install\">make install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: lfs-en/chapter08/man-db.xml:125
#, xml-text
msgid "Non-English Manual Pages in LFS"
msgstr "Páginas de Manual não inglesas no LFS"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/man-db.xml:127
#, xml-text
msgid ""
"The following table shows the character set that Man-DB assumes manual pages"
" installed under <filename "
"class=\"directory\">/usr/share/man/&lt;ll&gt;</filename> will be encoded "
"with.  In addition to this, Man-DB correctly determines if manual pages "
"installed in that directory are UTF-8 encoded."
msgstr ""
"A seguinte tabela mostra o conjunto de caracteres que o Man-DB supõe que as "
"páginas de manual instaladas sob <filename "
"class=\"directory\">/usr/share/man/&lt;ll&gt;</filename> estarão "
"codificadas. Em adição a isso, o Man-DB determina corretamente se páginas de"
" manual instaladas naquele diretório estão codificadas em UTF-8."

#. type: Content of: <sect1><sect2><table><title>
#: lfs-en/chapter08/man-db.xml:135
#, xml-text
msgid "Expected character encoding of legacy 8-bit manual pages"
msgstr ""
"Codificação de caracteres esperada das páginas de manual legadas de 8 bits"

#. type: Content of: <sect1><sect2><table>
#: lfs-en/chapter08/man-db.xml:136
#, xml-text
msgid "<?dbfo table-width=\"6in\" ?>"
msgstr "<?dbfo table-width=\"6in\" ?>"

#. type: Content of: <sect1><sect2><table><tgroup><thead><row><entry>
#: lfs-en/chapter08/man-db.xml:147 lfs-en/chapter08/man-db.xml:149
#, xml-text
msgid "Language (code)"
msgstr "Idioma (código)"

#. type: Content of: <sect1><sect2><table><tgroup><thead><row><entry>
#: lfs-en/chapter08/man-db.xml:148 lfs-en/chapter08/man-db.xml:150
#, xml-text
msgid "Encoding"
msgstr "Codificação"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:156
#, xml-text
msgid "Danish (da)"
msgstr "Dinamarquês (da)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:157 lfs-en/chapter08/man-db.xml:163
#: lfs-en/chapter08/man-db.xml:169 lfs-en/chapter08/man-db.xml:175
#: lfs-en/chapter08/man-db.xml:181 lfs-en/chapter08/man-db.xml:187
#: lfs-en/chapter08/man-db.xml:193 lfs-en/chapter08/man-db.xml:199
#: lfs-en/chapter08/man-db.xml:205 lfs-en/chapter08/man-db.xml:211
#: lfs-en/chapter08/man-db.xml:219 lfs-en/chapter08/man-db.xml:225
#: lfs-en/chapter08/man-db.xml:231 lfs-en/chapter08/man-db.xml:237
#: lfs-en/chapter08/man-db.xml:243 lfs-en/chapter08/man-db.xml:249
#: lfs-en/chapter08/man-db.xml:255 lfs-en/chapter08/man-db.xml:261
#, xml-text
msgid "ISO-8859-1"
msgstr "ISO-8859-1"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:158
#, xml-text
msgid "Croatian (hr)"
msgstr "Croata (hr)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:159 lfs-en/chapter08/man-db.xml:165
#: lfs-en/chapter08/man-db.xml:201 lfs-en/chapter08/man-db.xml:207
#: lfs-en/chapter08/man-db.xml:221 lfs-en/chapter08/man-db.xml:227
#: lfs-en/chapter08/man-db.xml:233 lfs-en/chapter08/man-db.xml:279
#, xml-text
msgid "ISO-8859-2"
msgstr "ISO-8859-2"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:162
#, xml-text
msgid "German (de)"
msgstr "Alemão (de)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:164
#, xml-text
msgid "Hungarian (hu)"
msgstr "Húngaro (hu)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:168
#, xml-text
msgid "English (en)"
msgstr "Inglês (en)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:170
#, xml-text
msgid "Japanese (ja)"
msgstr "Japonês (ja)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:171
#, xml-text
msgid "EUC-JP"
msgstr "EUC-JP"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:174
#, xml-text
msgid "Spanish (es)"
msgstr "Espanhol (es)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:176
#, xml-text
msgid "Korean (ko)"
msgstr "Coreano (ko)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:177
#, xml-text
msgid "EUC-KR"
msgstr "EUC-KR"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:180
#, xml-text
msgid "Estonian (et)"
msgstr "Estoniano (et)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:182
#, xml-text
msgid "Lithuanian (lt)"
msgstr "Lituano (lt)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:183 lfs-en/chapter08/man-db.xml:189
#, xml-text
msgid "ISO-8859-13"
msgstr "ISO-8859-13"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:186
#, xml-text
msgid "Finnish (fi)"
msgstr "Finlandês (fi)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:188
#, xml-text
msgid "Latvian (lv)"
msgstr "Letão (lv)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:192
#, xml-text
msgid "French (fr)"
msgstr "Francês (fr)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:194
#, xml-text
msgid "Macedonian (mk)"
msgstr "Macedônio (mk)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:195 lfs-en/chapter08/man-db.xml:239
#, xml-text
msgid "ISO-8859-5"
msgstr "ISO-8859-5"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:198
#, xml-text
msgid "Irish (ga)"
msgstr "Irlandês (ga)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:200
#, xml-text
msgid "Polish (pl)"
msgstr "Polonês (pl)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:204
#, xml-text
msgid "Galician (gl)"
msgstr "Galego (gl)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:206
#, xml-text
msgid "Romanian (ro)"
msgstr "Romeno (ro)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:210
#, xml-text
msgid "Indonesian (id)"
msgstr "Indonésio (id)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:212
#, xml-text
msgid "Greek (el)"
msgstr "Grego (el)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:213
#, xml-text
msgid "ISO-8859-7"
msgstr "ISO-8859-7"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:218
#, xml-text
msgid "Icelandic (is)"
msgstr "Islandês (is)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:220
#, xml-text
msgid "Slovak (sk)"
msgstr "Eslovaco (sk)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:224
#, xml-text
msgid "Italian (it)"
msgstr "Italiano (it)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:226
#, xml-text
msgid "Slovenian (sl)"
msgstr "Esloveno (sl)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:230
#, xml-text
msgid "Norwegian Bokmal (nb)"
msgstr "Bokmal norueguês (nb)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:232
#, xml-text
msgid "Serbian Latin (sr@latin)"
msgstr "Latim sérvio (sr@latin)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:236
#, xml-text
msgid "Dutch (nl)"
msgstr "Holandês (nl)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:238
#, xml-text
msgid "Serbian (sr)"
msgstr "Sérvio (sr)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:242
#, xml-text
msgid "Norwegian Nynorsk (nn)"
msgstr "Nynorsk norueguês (nn)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:244
#, xml-text
msgid "Turkish (tr)"
msgstr "Turco (tr)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:245
#, xml-text
msgid "ISO-8859-9"
msgstr "ISO-8859-9"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:248
#, xml-text
msgid "Norwegian (no)"
msgstr "Norueguês (no)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:250
#, xml-text
msgid "Ukrainian (uk)"
msgstr "Ucraniano (uk)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:251
#, xml-text
msgid "KOI8-U"
msgstr "KOI8-U"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:254
#, xml-text
msgid "Portuguese (pt)"
msgstr "Português (pt)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:256
#, xml-text
msgid "Vietnamese (vi)"
msgstr "Vietnamita (vi)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:257
#, xml-text
msgid "TCVN5712-1"
msgstr "TCVN5712-1"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:260
#, xml-text
msgid "Swedish (sv)"
msgstr "Sueco (sv)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:262
#, xml-text
msgid "Simplified Chinese (zh_CN)"
msgstr "Chinês simplificado (zh_CN)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:263 lfs-en/chapter08/man-db.xml:269
#, xml-text
msgid "GBK"
msgstr "GBK"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:266
#, xml-text
msgid "Belarusian (be)"
msgstr "Bielorrusso (be)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:267 lfs-en/chapter08/man-db.xml:273
#, xml-text
msgid "CP1251"
msgstr "CP1251"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:268
#, xml-text
msgid "Simplified Chinese, Singapore (zh_SG)"
msgstr "Chinês simplificado, Singapura (zh_SG)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:272
#, xml-text
msgid "Bulgarian (bg)"
msgstr "Búlgaro (bg)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:274
#, xml-text
msgid "Traditional Chinese, Hong Kong (zh_HK)"
msgstr "Chinês tradicional, Hong Kong (zh_HK)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:275
#, xml-text
msgid "BIG5HKSCS"
msgstr "BIG5HKSCS"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:278
#, xml-text
msgid "Czech (cs)"
msgstr "Tcheco (cs)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:280
#, xml-text
msgid "Traditional Chinese (zh_TW)"
msgstr "Chinês tradicional (zh_TW)"

#. type: Content of: <sect1><sect2><table><tgroup><tbody><row><entry>
#: lfs-en/chapter08/man-db.xml:281
#, xml-text
msgid "BIG5"
msgstr "BIG5"

#. type: Content of: <sect1><sect2><note><para>
#: lfs-en/chapter08/man-db.xml:297
#, xml-text
msgid "Manual pages in languages not in the list are not supported."
msgstr ""
"Páginas de manual em idiomas que não estão na lista não são suportadas."

#. type: Content of: <sect1><sect2><title>
#: lfs-en/chapter08/man-db.xml:303
#, xml-text
msgid "Contents of Man-DB"
msgstr "Conteúdo do Man-DB"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/man-db.xml:306
#, xml-text
msgid "Installed programs"
msgstr "Aplicativos instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/man-db.xml:307
#, xml-text
msgid "Installed libraries"
msgstr "Bibliotecas instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/man-db.xml:308
#, xml-text
msgid "Installed directories"
msgstr "Diretórios instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/man-db.xml:311
#, xml-text
msgid ""
"accessdb, apropos (link to whatis), catman, lexgrog, man, man-recode, mandb,"
" manpath, and whatis"
msgstr ""
"accessdb, apropos (link para whatis), catman, lexgrog, man, man-recode, "
"mandb, manpath e whatis"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/man-db.xml:313
#, xml-text
msgid "libman.so and libmandb.so (both in /usr/lib/man-db)"
msgstr "libman.so e libmandb.so (ambas em /usr/lib/man-db)"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/man-db.xml:314
#, xml-text
msgid ""
"/usr/lib/man-db, /usr/libexec/man-db, and /usr/share/doc/man-db-&man-db-"
"version;"
msgstr ""
"/usr/lib/man-db, /usr/libexec/man-db e /usr/share/doc/man-db-&man-db-"
"version;"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: lfs-en/chapter08/man-db.xml:320
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: lfs-en/chapter08/man-db.xml:321
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:325
#, xml-text
msgid "<command>accessdb</command>"
msgstr "<command>accessdb</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:327
#, xml-text
msgid ""
"Dumps the <command>whatis</command> database contents in human-readable form"
msgstr ""
"Despeja o conteúdo da base de dados <command>whatis</command> em formato "
"legível por humanos"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:330
#, xml-text
msgid "accessdb"
msgstr "accessdb"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:336
#, xml-text
msgid "<command>apropos</command>"
msgstr "<command>apropos</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:338
#, xml-text
msgid ""
"Searches the <command>whatis</command> database and displays the short "
"descriptions of system commands that contain a given string"
msgstr ""
"Pesquisa na base de dados <command>whatis</command> e exibe as descrições "
"curtas dos comandos de sistema que contém uma sequência de caracteres dada"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:342
#, xml-text
msgid "apropos"
msgstr "apropos"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:348
#, xml-text
msgid "<command>catman</command>"
msgstr "<command>catman</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:350
#, xml-text
msgid "Creates or updates the pre-formatted manual pages"
msgstr "Cria ou atualiza páginas de manual pré-formatadas"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:352
#, xml-text
msgid "catman"
msgstr "catman"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:358
#, xml-text
msgid "<command>lexgrog</command>"
msgstr "<command>lexgrog</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:360
#, xml-text
msgid "Displays one-line summary information about a given manual page"
msgstr ""
"Exibe informação de sumário em uma linha a respeito de uma página de manual "
"dada"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:363
#, xml-text
msgid "lexgrog"
msgstr "lexgrog"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:369
#, xml-text
msgid "<command>man</command>"
msgstr "<command>man</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:371
#, xml-text
msgid "Formats and displays the requested manual page"
msgstr "Formata e exibe a página de manual solicitada"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:373
#, xml-text
msgid "man"
msgstr "man"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:379
#, xml-text
msgid "<command>man-recode</command>"
msgstr "<command>man-recode</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:381
#, xml-text
msgid "Converts manual pages to another encoding"
msgstr "Converte páginas de manual para outra codificação"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:383
#, xml-text
msgid "man-recode"
msgstr "man-recode"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:389
#, xml-text
msgid "<command>mandb</command>"
msgstr "<command>mandb</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:391
#, xml-text
msgid "Creates or updates the <command>whatis</command> database"
msgstr "Cria ou atualiza a base de dados <command>whatis</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:393
#, xml-text
msgid "mandb"
msgstr "mandb"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:399
#, xml-text
msgid "<command>manpath</command>"
msgstr "<command>manpath</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:401
#, xml-text
msgid ""
"Displays the contents of $MANPATH or (if $MANPATH is not set)  a suitable "
"search path based on the settings in man.conf and the user's environment"
msgstr ""
"Exibe o conteúdo de $MANPATH ou (se $MANPATH não estiver configurada) um "
"caminho de busca adequado baseado nas configurações em man.conf e no "
"ambiente do(a) usuário(a)"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:405
#, xml-text
msgid "manpath"
msgstr "manpath"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:411
#, xml-text
msgid "<command>whatis</command>"
msgstr "<command>whatis</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:413
#, xml-text
msgid ""
"Searches the <command>whatis</command> database and displays the short "
"descriptions of system commands that contain the given keyword as a separate"
" word"
msgstr ""
"Pesquisa na base de dados <command>whatis</command> e exibe as descrições "
"curtas de comandos do sistema que contenham a palavra chave dada como uma "
"palavra separada"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:417
#, xml-text
msgid "whatis"
msgstr "whatis"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:423
#, xml-text
msgid "<filename class=\"libraryfile\">libman</filename>"
msgstr "<filename class=\"libraryfile\">libman</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/man-db.xml:425 lfs-en/chapter08/man-db.xml:435
#, xml-text
msgid "Contains run-time support for <command>man</command>"
msgstr "Contém suporte em tempo de execução para o <command>man</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:427
#, xml-text
msgid "libman"
msgstr "libman"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/man-db.xml:433
#, xml-text
msgid "<filename class=\"libraryfile\">libmandb</filename>"
msgstr "<filename class=\"libraryfile\">libmandb</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/man-db.xml:437
#, xml-text
msgid "libmandb"
msgstr "libmandb"

#, xml-text
#~ msgid "One test named <filename>man1/lexgrog.1</filename> is known to fail."
#~ msgstr ""
#~ "Um teste chamado <filename>man1/lexgrog.1</filename> é conhecido por falhar."

#, xml-text
#~ msgid ""
#~ "Many tests are known to fail with groff-1.23.0 or later.  If you still want "
#~ "to test the results anyway, issue:"
#~ msgstr ""
#~ "Muitos testes são conhecidos por falharem com groff-1.23.0 ou posterior. Se "
#~ "você ainda quiser testar os resultados de qualquer maneira, [então] emita:"
