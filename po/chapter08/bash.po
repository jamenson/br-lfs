# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-03-05 01:51-0300\n"
"PO-Revision-Date: 2024-11-09 04:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/linux-from-scratch/chapter08_bash/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.7.2\n"

#. type: Content of: <sect1><sect1info><address>
#: lfs-en/chapter08/bash.xml:14
#, no-wrap, xml-text
msgid "&bash-url;"
msgstr "&bash-url;"

#. type: Content of: <sect1><sect1info>
#: lfs-en/chapter08/bash.xml:12
#, xml-text
msgid ""
"<productname>bash</productname> <productnumber>&bash-"
"version;</productnumber> <placeholder type=\"address\" id=\"0\"/>"
msgstr ""
"<productname>bash</productname> <productnumber>&bash-"
"version;</productnumber> <placeholder type=\"address\" id=\"0\"/>"

#. type: Content of: <sect1><title>
#: lfs-en/chapter08/bash.xml:17
#, xml-text
msgid "Bash-&bash-version;"
msgstr "Bash-&bash-version;"

#. type: Content of: <sect1><indexterm><primary>
#: lfs-en/chapter08/bash.xml:20
#, xml-text
msgid "Bash"
msgstr "Bash"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/bash.xml:26
#, xml-text
msgid "The Bash package contains the Bourne-Again Shell."
msgstr "O pacote Bash contém o Bourne-Again SHell."

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/bash.xml:29
#, xml-text
msgid "&buildtime;"
msgstr "&buildtime;"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/bash.xml:30
#, xml-text
msgid "&diskspace;"
msgstr "&diskspace;"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/bash.xml:33
#, xml-text
msgid "&bash-fin-sbu;"
msgstr "&bash-fin-sbu;"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/bash.xml:34
#, xml-text
msgid "&bash-fin-du;"
msgstr "&bash-fin-du;"

#. type: Content of: <sect1><sect2><title>
#: lfs-en/chapter08/bash.xml:41
#, xml-text
msgid "Installation of Bash"
msgstr "Instalação do Bash"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/bash.xml:43
#, xml-text
msgid "Prepare Bash for compilation:"
msgstr "Prepare o Bash para compilação:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/bash.xml:45
#, no-wrap, xml-text
msgid ""
"<userinput remap=\"configure\">./configure --prefix=/usr             \\\n"
"            --without-bash-malloc     \\\n"
"            --with-installed-readline \\\n"
"            --docdir=/usr/share/doc/bash-&bash-version;</userinput>"
msgstr ""
"<userinput remap=\"configure\">./configure --prefix=/usr             \\\n"
"            --without-bash-malloc     \\\n"
"            --with-installed-readline \\\n"
"            --docdir=/usr/share/doc/bash-&bash-version;</userinput>"

#. type: Content of: <sect1><sect2><variablelist><title>
#: lfs-en/chapter08/bash.xml:51
#, xml-text
msgid "The meaning of the new configure option:"
msgstr "O significado da nova opção do configure:"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/bash.xml:54
#, xml-text
msgid "<parameter>--with-installed-readline</parameter>"
msgstr "<parameter>--with-installed-readline</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/bash.xml:56
#, xml-text
msgid ""
"This option tells Bash to use the <filename "
"class=\"libraryfile\">readline</filename> library that is already installed "
"on the system rather than using its own readline version."
msgstr ""
"Essa opção diz ao Bash para usar a biblioteca <filename "
"class=\"libraryfile\">readline</filename> que já está instalada no sistema "
"em vez de usar a própria versão dele da readline."

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/bash.xml:65
#, xml-text
msgid "Compile the package:"
msgstr "Compile o pacote:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/bash.xml:67
#, no-wrap, xml-text
msgid "<userinput remap=\"make\">make</userinput>"
msgstr "<userinput remap=\"make\">make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/bash.xml:69
#, xml-text
msgid ""
"Skip down to <quote>Install the package</quote> if not running the test "
"suite."
msgstr ""
"Pule para <quote>Instale o pacote</quote> se não executar a suíte de teste."

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/bash.xml:72
#, xml-text
msgid ""
"To prepare the tests, ensure that the <systemitem "
"class=\"username\">tester</systemitem> user can write to the sources tree:"
msgstr ""
"Para preparar os testes, garanta que o(a) usuário(a) <systemitem "
"class=\"username\">tester</systemitem> consegue escrever na árvore dos "
"fontes:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/bash.xml:74
#, no-wrap, xml-text
msgid "<userinput remap=\"test\">chown -R tester .</userinput>"
msgstr "<userinput remap=\"test\">chown -R tester .</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/bash.xml:76
#, xml-text
msgid ""
"The test suite of this package is designed to be run as a non-&root; user "
"who owns the terminal connected to standard input.  To satisfy the "
"requirement, spawn a new pseudo terminal using "
"<application>Expect</application> and run the tests as the <systemitem "
"class=\"username\">tester</systemitem> user:"
msgstr ""
"A suíte de teste desse pacote é projetada para ser executada como um(a) "
"usuário(a) não &root; que é proprietário(a) do terminal conectado à entrada "
"padrão. Para satisfazer a exigência, gere um novo pseudo terminal usando o "
"<application>Expect</application> e execute os testes como o(a) usuário(a) "
"<systemitem class=\"username\">tester</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/bash.xml:82
#, no-wrap, xml-text
msgid ""
"<userinput remap=\"test\">su -s /usr/bin/expect tester &lt;&lt; \"EOF\"\n"
"set timeout -1\n"
"spawn make tests\n"
"expect eof\n"
"lassign [wait] _ _ _ value\n"
"exit $value\n"
"EOF</userinput>"
msgstr ""
"<userinput remap=\"test\">su -s /usr/bin/expect tester &lt;&lt; \"EOF\"\n"
"set timeout -1\n"
"spawn make tests\n"
"expect eof\n"
"lassign [wait] _ _ _ value\n"
"exit $value\n"
"EOF</userinput>"

#.  Some host distros set core file size hard limit < 1000, then the
#.          test "ulimit -c -S 1000" attempts to set soft limit >
#.  hard limit
#.          and fail.
#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/bash.xml:90
#, xml-text
msgid ""
"The test suite uses <command>diff</command> to detect the difference between"
" test script output and the expected output.  Any output from "
"<command>diff</command> (prefixed with <computeroutput>&lt;</computeroutput>"
" and <computeroutput>&gt;</computeroutput>) indicates a test failure, unless"
" there is a message saying the difference can be ignored.  One test named "
"<filename>run-builtins</filename> is known to fail on some host distros with"
" a difference on the first line of the output."
msgstr ""
"A suíte de teste usa o <command>diff</command> para detectar a diferença "
"entre a saída gerada do script de teste e a saída gerada esperada. Qualquer "
"saída gerada oriunda do <command>diff</command> (prefixada com "
"<computeroutput>&lt;</computeroutput> e "
"<computeroutput>&gt;</computeroutput>) indica uma falha de teste, a menos "
"que exista uma mensagem dizendo que a diferença pode ser ignorada. Um teste "
"chamado <filename>run-builtins</filename> é conhecido por falhar em algumas "
"distribuições anfitriãs com uma diferença na primeira linha da saída gerada."

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/bash.xml:103
#, xml-text
msgid "Install the package:"
msgstr "Instale o pacote:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/bash.xml:105
#, no-wrap, xml-text
msgid "<userinput remap=\"install\">make install</userinput>"
msgstr "<userinput remap=\"install\">make install</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/bash.xml:107
#, xml-text
msgid ""
"Run the newly compiled <command>bash</command> program (replacing the one "
"that is currently being executed):"
msgstr ""
"Execute o aplicativo recém compilado <command>bash</command> (substituindo o"
" que está sendo executado atualmente):"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/bash.xml:110
#, no-wrap, xml-text
msgid "<userinput>exec /usr/bin/bash --login</userinput>"
msgstr "<userinput>exec /usr/bin/bash --login</userinput>"

#. type: Content of: <sect1><sect2><title>
#: lfs-en/chapter08/bash.xml:115
#, xml-text
msgid "Contents of Bash"
msgstr "Conteúdo do Bash"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/bash.xml:118
#, xml-text
msgid "Installed programs"
msgstr "Aplicativos instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/bash.xml:119
#, xml-text
msgid "Installed directory"
msgstr "Diretório instalado"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/bash.xml:122
#, xml-text
msgid "bash, bashbug, and sh (link to bash)"
msgstr "bash, bashbug e sh (link para bash)"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/bash.xml:123
#, xml-text
msgid ""
"/usr/include/bash, /usr/lib/bash, and /usr/share/doc/bash-&bash-version;"
msgstr "/usr/include/bash, /usr/lib/bash e /usr/share/doc/bash-&bash-version;"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: lfs-en/chapter08/bash.xml:129
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: lfs-en/chapter08/bash.xml:130
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/bash.xml:134
#, xml-text
msgid "<command>bash</command>"
msgstr "<command>bash</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/bash.xml:136
#, xml-text
msgid ""
"A widely-used command interpreter; it performs many types of expansions and "
"substitutions on a given command line before executing it, thus making this "
"interpreter a powerful tool"
msgstr ""
"Um interpretador de comandos vastamente usado; ele realiza muitos tipos de "
"expansões e substituições sobre uma dada linha de comando antes de executá-"
"la, portanto fazendo desse interpretador uma ferramenta poderosa"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/bash.xml:140
#, xml-text
msgid "bash"
msgstr "bash"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/bash.xml:146
#, xml-text
msgid "<command>bashbug</command>"
msgstr "<command>bashbug</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/bash.xml:148
#, xml-text
msgid ""
"A shell script to help the user compose and mail standard formatted bug "
"reports concerning <command>bash</command>"
msgstr ""
"Um script de shell para ajudar o(a) usuário(a) a compor e enviar relatórios "
"de defeitos formatados padrão concernentes ao <command>bash</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/bash.xml:151
#, xml-text
msgid "bashbug"
msgstr "bashbug"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/bash.xml:157
#, xml-text
msgid "<command>sh</command>"
msgstr "<command>sh</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/bash.xml:159
#, xml-text
msgid ""
"A symlink to the <command>bash</command> program; when invoked as "
"<command>sh</command>, <command>bash</command> tries to mimic the startup "
"behavior of historical versions of <command>sh</command> as closely as "
"possible, while conforming to the POSIX standard as well"
msgstr ""
"Um link simbólico para o aplicativo <command>bash</command>; quando invocado"
" como <command>sh</command>, o <command>bash</command> tenta imitar o "
"comportamento de inicialização de versões históricas do "
"<command>sh</command> o mais próximo possível, enquanto conformante ao "
"padrão POSIX também"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/bash.xml:165
#, xml-text
msgid "sh"
msgstr "sh"

#, xml-text
#~ msgid "First, fix some issues identified upstream:"
#~ msgstr ""
#~ "Primeiro, corrija alguns problemas identificados pelo(a) desenvolvedor(a):"

#, no-wrap, xml-text
#~ msgid ""
#~ "<userinput remap=\"pre\">patch -Np1 -i ../&bash-upstream-fixes-"
#~ "patch;</userinput>"
#~ msgstr ""
#~ "<userinput remap=\"pre\">patch -Np1 -i ../&bash-upstream-fixes-"
#~ "patch;</userinput>"
